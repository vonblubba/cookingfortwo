@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Recipe management
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="/admin">Admin</a>
                </li>
                <li class="active">
                    <i class="fa fa-book"></i> Recipes
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
     <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Search recipe</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('action' => 'RecipeController@index', 'class' => 'form-horizontal', 'method' => 'GET')) !!}
                        
                        <div class="form-group">
                            {!! Form::label('recipe-name', 'Name', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('order-by', 'Order by', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::select('order_by', ['name' => 'Name', 'recipe_pub_date' => 'Pub date'], Input::old('order_by'), ['class' => 'col-sm-3 form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                        {!! Form::submit('Search', array('class' => 'btn btn-default')) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    
    @if (count($recipes) > 0)
    <div class="row">
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <!-- Display Flash Messages -->
            @include('common.flash')
            
            <div class="table-responsive">
                <br>
                <table id="admintable" class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Pub Date</th>
                            <th>Pub</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($recipes as $recipe)
                        <tr> 
                            <td class="table-text">
                                <div>{{ $recipe->name }}</div>
                            </td>
                            
                            <td class="table-text text-center">
                                {{ $recipe->pub_date }}
                            </td>
                            <td class="table-text text-center">
                                @if($recipe->published)
                                    <div><i class="fa fa-check"></i></div>
                                @else
                                    <div><i class="fa fa-times"></i></div>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group">
                                    {!! link_to_route('recipe.show', 'Edit', array('id' => $recipe->id), array('class' => 'btn btn-primary btn-sm')) !!}
                                </div>
                                <form action="/recipe/{{ $recipe->id }}" method="POST" style = "display: inline;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $recipes->render() !!} 
            </div>
        </div>
    </div>
    <!-- /.row -->
    @endif
@endsection
