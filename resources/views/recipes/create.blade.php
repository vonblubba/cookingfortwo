@extends('layouts.admin')

@section('content')
    {!! HTML::script('assets/plugins/ckeditor/ckeditor.js') !!}
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                New recipe
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="/admin">Admin</a>
                </li>
                <li>
                    <i class="fa fa-book"></i>  <a href="/recipe">Recipes</a>
                </li>
                <li class="active">
                    <i class="fa fa-plus-circle"></i> New Recipe
                </li>
            </ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Insert recipe data</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('action' => 'RecipeController@store', 'files'=>true, 'class' => 'form-horizontal')) !!}
                        
                        <div class="form-group">
                            {!! Form::label('recipe-ingredient-0', 'Ingredient', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::text('ingredient[0][quantity]', null, array('class' => 'form-control')) !!}
                                {!! Form::select('ingredient[0][id]', $ingredient_list, null, ['class' => 'col-sm-3 form-control']) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-ingredient-1', 'Ingredient', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::text('ingredient[1][quantity]', null, array('class' => 'form-control')) !!}
                                {!! Form::select('ingredient[1][id]', $ingredient_list, null, ['class' => 'col-sm-3 form-control']) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-ingredient-2', 'Ingredient', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::text('ingredient[2][quantity]', null, array('class' => 'form-control')) !!}
                                {!! Form::select('ingredient[2][id]', $ingredient_list, null, ['class' => 'col-sm-3 form-control']) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-name', 'Name (*)', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-pub_date', 'Publication date', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::text('pub_date', Input::old('pub_date'), array('id' => 'datepicker', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-published', 'Published', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::checkbox('published', 1, Input::old('published')) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-step1', 'Step 1', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::textarea('step[0]', Input::old('step[0]'), array('class' => 'form-control ckeditor', 'rows' => 4, 'cols' => 50)) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-step2', 'Step 2', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::textarea('step[1]', Input::old('step[0]'), array('class' => 'form-control ckeditor', 'rows' => 4, 'cols' => 50)) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-step3', 'Step 3', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::textarea('step[2]', Input::old('step[2]'), array('class' => 'form-control ckeditor', 'rows' => 4, 'cols' => 50)) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('recipe-step4', 'Step 4', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-6">
                                {!! Form::textarea('step[3]', Input::old('step[3]'), array('class' => 'form-control ckeditor', 'rows' => 4, 'cols' => 50)) !!}
                            </div>
                        </div>
    
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                            <a class="btn btn-default btn-close" href="{{ route('recipe.index') }}">Annulla</a>
                            {!! Form::submit('Save', array('class' => 'btn btn-default')) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
