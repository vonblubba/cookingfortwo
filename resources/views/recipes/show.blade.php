@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                {{ $recipe->name }}
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="/admin">Admin</a>
                </li>
                <li>
                    <i class="fa fa-book"></i>  <a href="/book">Recipes</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> {{ $recipe->name }}
                </li>
            </ol>
        </div>
    </div>
    
    <div class="row">
        <!-- Display Flash Messages -->
        @include('common.flash')
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Recipe data</h3>
                </div>
                <div class="panel-body">
                    <div><strong>Name:</strong> <br> <span>{{ $recipe->name }}</span></div><br>
                    <div><strong>Pub. date:</strong> <br> <span> {{ $recipe->pub_date  or "-" }}</span></div><br>
                    <div><strong>Published:</strong> <br> <span> {{ ($recipe->published) ? 'YES' : 'NO'  }}</span></div><br>
                    <div>{!! link_to_route('recipe.edit', 'Edit', array('id' => $recipe->id), array('class' => 'btn btn-primary btn-sm')) !!}</div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Ingredients</h3>
                </div>
                <div class="panel-body">
                    @foreach ($recipe->ingredients as $ingredient)
                        <div><span>{{ $ingredient->name  or "-"  }}</span></div><br> 
                    @endforeach
                    <div>{!! link_to_route('recipe.editIngredients', 'Edit', array('recipe' => $recipe->id), array('class' => 'btn btn-primary btn-sm')) !!}</div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <h3>Steps</h3>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Body</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($recipe->steps as $step)
                        <tr> 
                            <td class="table-text">
                                <div>{{ $step->body }}</div>
                            </td>

                            <td>
                                <div class="btn-group">
                                    {!! link_to_route('step.edit', 'Edit', array('recipe' => $recipe->id, 'step' => $step->id), array('class' => 'btn btn-primary btn-sm')) !!}
                                </div>
                                <form action="/step/{{ $step->id }}" method="POST" style = "display: inline;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div>
                    {!! link_to_route('step.create', 'Add step', array('id' => $recipe->id), array('class' => 'btn btn-primary btn-sm')) !!}
                </div>
            </div>
        </div>
    </div>
@endsection