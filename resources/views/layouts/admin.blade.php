<!DOCTYPE html>
<html lang="it">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>cookingfortwo.it - Administration</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/admin_layout/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/css/admin_layout/sb-admin.css" rel="stylesheet">
    <link href="/assets/css/admin_layout/custom.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <link href="/assets/js/admin_layout/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css">
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin">cookingfortwo.it - Administration</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    @if (!Auth::guest())
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }} <b class="caret"></b></a>
                    @endif
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/auth/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <!--<li>
                        <a href="\admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li> -->
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#recipes"><i class="fa fa-fw fa-book"></i> Recipes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="recipes" class="collapse">
                            <li>
                                <a href="{{ route('recipe.index') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('recipe.create') }}">New recipe</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#ingredients"><i class="fa fa-fw fa-pencil"></i> Ingredients <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="ingredients" class="collapse">
                            <li>
                                <a href="{{ route('ingredient.index') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('ingredient.create') }}">New ingredient</a>
                            </li>
                        </ul>
                    </li>
 
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">
                
                @yield('content')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/assets/plugins/jquery/jquery.min.js"></script>
    <script src="/assets/js/admin_layout/jquery-ui/jquery-ui.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/assets/js/admin_layout/bootstrap.min.js"></script>
    
    <script src="/assets/js/admin_layout/custom.js"></script>
    
    <script src="/assets/js/admin_layout/jquery.tablesorter/jquery.tablesorter.js"></script>
    
    <script>
        $(document).ready(function() 
            { 
                $("#admintable").tablesorter({sortList: [[3,1], [0,1]]}); 
            } 
        ); 
    
    </script>

</body>

</html>