<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        
        Schema::create('ingredient_recipe', function (Blueprint $table) {
            $table->integer('ingredient_id');
            $table->integer('recipe_id');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredients');
        Schema::drop('ingredient_recipe');
    }
}
