<?php
namespace App\Repositories;
use App\Recipe;
use App\Ingredient;
use Carbon\Carbon;
use DB;

class IngredientRepository
{
    public function getAll($perPage = 15)
    {
        return Ingredient::orderBy('name', 'asc')->paginate($perPage);
    }
}
