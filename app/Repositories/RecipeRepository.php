<?php
namespace App\Repositories;
use App\Recipe;
use Carbon\Carbon;
use DB;

class RecipeRepository
{

    public function getAll($perPage = 15)
    {
        return Recipe::orderBy('pub_date', 'desc')->paginate($perPage);
    }
    
    public function getLatest($limit = 8)
    {
        return Recipe::published(true)
            ->orderBy('pub_date', 'desc')
            ->take($limit)->get();
    }
    
    public function search($searchString) 
    {
        
    }
}
