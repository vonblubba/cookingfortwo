<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = ['name', 'published', 'pub_date'];
    
    public function steps()
    {
        return $this->hasMany('App\Step');
    }
    
    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient');
    }
}
