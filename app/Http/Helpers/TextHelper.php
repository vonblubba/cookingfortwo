<?php

namespace App\Helpers;
use DateTime;

class TextHelper
{
    public static function shorten($text, $maxChars = 25, $postfix = '&hellip;')
    {
        $parts = preg_split('/([\s\n\r]+)/', $text, null, PREG_SPLIT_DELIM_CAPTURE);
        $parts_count = count($parts);
    
        $length = 0;
        $last_part = 0;
        for (; $last_part < $parts_count; ++$last_part) {
          $length += strlen($parts[$last_part]);
          if ($length > $maxChars) { break; }
        }
        
        if ($length > $maxChars) { 
            return implode(array_slice($parts, 0, $last_part)) . $postfix;
        } else {
            return implode(array_slice($parts, 0, $last_part));
        }
    }
    
    public static function formatDate($date) 
    {
        $datef = new DateTime($date);
        return $datef->format('jS F Y');
    }
    
    public static function formatDateForTable($date) 
    {
        $datef = new DateTime($date);
        return $datef->format('Y-m-d');
    }
}