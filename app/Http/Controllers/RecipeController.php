<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Recipe;
use App\Ingredient;
use App\Step;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\RecipeRepository;
use App\Http\Requests\StoreRecipeRequest;
use Session;
use Image;
use Log;
use Input;
use DB;
use Auth;

class RecipeController extends Controller
{
    protected $recipes;

    public function __construct(RecipeRepository $recipes)
    {
        //$this->middleware('auth');
        $this->recipes = $recipes;
    }
    
    public function index(Request $request)
    {
        $results = Recipe::paginate(50);

        return view('recipes.index')
            ->with('recipes', $results->appends(Input::except('page')));
    }
    
    public function show(Request $request, Recipe $recipe)
    {
        return view('recipes.show')
            ->with('recipe', $recipe);
    }

    public function create(Request $request)
    {
    	$ingredient_list = Ingredient::orderBy('name')->get()->lists('name', 'id');
        return view('recipes.create')->with('ingredient_list', $ingredient_list);
    }
    
    public function store(StoreRecipeRequest $request)
    {
        $recipe = new Recipe($request->all());
        $recipe->save();
        
        if (Input::has('ingredient')) {
            /*$recipe->ingredients()->sync(Input::get('ingredient_id'));*/
            foreach(Input::get('ingredient') as $ingredient)
            {
                $recipe->ingredients()->attach($ingredient['id'], ['quantity' => $ingredient['quantity']]);
            }
        }
        
        if (Input::has('step')) {
            foreach(Input::get('step') as $step)
            {
                $new_step = new Step(array('body' => $step));
                $new_step->recipe_id = $recipe->id;
                $new_step->save();
            }
        }
        
        Session::flash('message', 'Recipe created');
        return redirect()->route('recipe.show', array($recipe->id));
    }
    
    public function editIngredients(Request $request, Recipe $recipe)
    {
        return view('recipe.editIngredients')
            ->with('recipe', $recipe);
    }
}
